window.App =
	run: ->
		$ = require('jquery')
		window.jQuery ?= $

		Suggest = require('./Suggest')

		$('.js-suggest')
			.each(->
				suggest = new Suggest(this).init()
				fn = ->
					console.log('test event')
				suggest
					.on('test', fn)
					.trigger('test')
					.off('test', fn)
				console.log(suggest)

				suggest
					.on('suggeststart', ->
						console.log('start')
					)
					.on('suggestend', ->
						console.log('end')
					)
					.on('typestart', ->
						console.log('typestart')
					)
					.on('typeend', ->
						console.log('typeend')
					)
					.on('typeclear', ->
						console.log('typeclear')
					)
					.on('beforesend', (payload) ->
						console.log('before', payload)
					)
					.on('success', (payload) ->
						console.log('success', payload)
					)
			)

		return

