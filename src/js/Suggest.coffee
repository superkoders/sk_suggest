$ = require('jquery')
Events = require('sk-events')

class Suggest extends Events
	constructor: (element, options) ->
		super
		@$input = if element.jquery then element else $(element)
		@options = $.extend({
			minLength: 2
			typeInterval: 500
			url: '/'
			loadingClass: 'is-loading'
			typingClass: 'is-typing'
		}, options)
		@isResult = false
		@typeTimer = null
		@searchTerm = ''
		@ajax = []

		@isInit = false

	init: ->
		if @isInit
			return this

		@$input
			.attr('autocomplete', 'off')
			.on('keydown', @handleKeydown)
			.on('keypress', @handleKeypress)
			.on('focus', @handleFocus)
			.on('blur', @handleBlur)

		@isInit = true
		return this


	destroy: ->
		unless @isInit
			return this

		@$input
			.off('keydown', @handleKeydown)
			.off('keypress', @handleKeypress)
			.off('focus', @handleFocus)
			.off('blur', @handleBlur)

		@isInit = false
		return this

	handleFocus: =>
		@trigger('suggeststart')
		@type()

	handleBlur: =>
		@trigger('suggestend')
		@$input.removeClass(@options.typingClass)

	handleKeydown: (event) =>
		@typeTimer = clearTimeout(@typeTimer)
		@typeTimer = setTimeout(=>
			@type()
		, @options.typeInterval)

		@$input.addClass(@options.typingClass)
		@trigger('typestart',
			event: event
		)

	handleKeypress: =>
		if @$input.val() isnt @searchTerm
			@ajaxAbort()

	ajaxRemove: (instance) ->
		for ajax, index in @ajax
			if ajax is instance
				@ajax.splice(index, 1)
				break

	ajaxAbort: ->
		for ajax in @ajax
			ajax.abort()

	type: ->
		value = @$input.val()
		o = @options

		if value isnt @searchTerm
			@searchTerm = value
			if value.length >= o.minLength
				@search()
			else if @isResult
				@isResult = false
				@ajaxAbort()
				@trigger('typeclear')
			else
				@trigger('typeend')
		else
			@trigger('typeend')

		@$input.removeClass(o.typingClass)

	search: ->
		@ajaxAbort()
		# Sestavení dat k poslání na server
		value = @$input.val()
		o = @options
		data = {}
		data[@$input.attr('name')] = value

		@ajax.push(
			$.ajax(
				type: 'GET'
				url: o.url
				data: data
				beforeSend: (jqXHR, settings) =>
					@trigger('beforesend',
						jqXHR: jqXHR
						settings: settings
					)
					@$input.addClass(o.loadingClass)

				success: (respond, textStatus, jqXHR) =>
					@ajaxRemove(jqXHR)
					@trigger('success',
						respond: respond
						textStatus: textStatus
						jqXHR: jqXHR
					)
					@$input.removeClass(o.loadingClass)
					@isResult = true

				error: (jqXHR, textStatus, errorThrown) =>
					@ajaxRemove(jqXHR)
					@$input.removeClass(o.loadingClass)
			)
		)
		return

	clear: ->
		@$input.val('')
		@typeTimer = clearTimeout(@typeTimer)
		@type()


module.exports = Suggest
