### 0.0.5 / 04.02.2015
- dědičnost konstruktoru přesunuta na začátek
- přidána kontrola zda je instance inicializovaná aby nedošlo k opětovné inicializaci
- při vyvolání události **typestart** se nyní předává celý event objekt **keydown** události

### 0.0.4 / 04.02.2015
- Změna git url na **sk-events** v bower.json

### 0.0.3 / 04.02.2015
- Přidána závislost na **sk-events**.
- Zdědění **sk-events**.
- Změna vyvolávání událostí. Odstranění jQuery vazby. Vyvolání událostí pomocí zděděných metod.
- přejmenování metod listenerů dle SuperKodéří konvence.
- změna defaultní hodnoty **options.loadingClass** z **'loading'** na **'is-loading'**. Dle BEM konvence.
- změna defaultní hodnoty **options.typingClass** z **'typing'** na **'is-typing'**. Dle BEM konvence.
- Přidání ignore do bower.json

### 0.0.2 / 03.02.2015
- Oprava formátu bower.json

### 0.0.1 / 03.02.2015
- vytvoření
